/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clicktours;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * Esta clase llena la lsita Habitaciones desde el archivo
 * @author Gilson Ponce Briones
 */
public class Habitacion {
     public static List<Habitacion> habitaciones = new ArrayList<>();
     private int idHabitacion;
     private int idHotel;
     private String tipoHabitacion;
     private double tarifaSim;
     private double tarifaDou;
     private double tarifaTri;

    public Habitacion(int idHabitacion, int idHotel, String tipoHabitacion, double tarifaSim, double tarifaDou, double tarifaTri) {
        this.idHabitacion = idHabitacion;
        this.idHotel = idHotel;
        this.tipoHabitacion = tipoHabitacion;
        this.tarifaSim = tarifaSim;
        this.tarifaDou = tarifaDou;
        this.tarifaTri = tarifaTri;
    }

    public int getIdHabitacion() {
        return idHabitacion;
    }

    public int getIdHotel() {
        return idHotel;
    }

    public String getTipoHabitacion() {
        return tipoHabitacion;
    }

    public double getTarifaSim() {
        return tarifaSim;
    }

    public double getTarifaDou() {
        return tarifaDou;
    }

    public double getTarifaTri() {
        return tarifaTri;
    }
    
     /*Este metodo llena la lista de habitaciones desde el archivo*/
    public static void llenarListaHabitaciones() throws IOException{
        try(BufferedReader r = new BufferedReader(new FileReader("src/datos/habitaciones.csv"));){
            String Linea;
            r.readLine();
            while((Linea = r.readLine()) != null ){
                String[] s = Linea.split("\\|");
                int idHabi = Integer.parseInt(s[0]);
                int idHotel = Integer.parseInt(s[1]);
                double tarifaSi = Double.parseDouble(s[3]);
                double tarifaDo = Double.parseDouble(s[4]);
                double tarifaTri = Double.parseDouble(s[5]);
                habitaciones.add(new Habitacion(idHabi,idHotel,s[2],tarifaSi,tarifaDo,tarifaTri));
            } 
        }catch(FileNotFoundException ex){
            System.out.println("No se encontro el archivo habitaciones.csv "+ex);            
        }
    }
    
    /*este metodo entrega las habitaciones que hay en un hotel determinado*/
    public static List<Habitacion> listaHabiHotel(int idhote){
        List<Habitacion> habitacioneshotel = new ArrayList<>();
        for(Habitacion ha : habitaciones){
            if(ha.idHotel == idhote){
                habitacioneshotel.add(ha);
            }
        }
        return habitacioneshotel;
    }

    @Override
    public String toString() {
        return tipoHabitacion;
    }
    
    
}
