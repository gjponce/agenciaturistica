/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clicktours;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * Esta calse llena la lista de hoteles desde el archivo
 * @author Gilson Ponce Briones
 */
public class Hotel {
     public static List<Hotel> hoteles = new ArrayList<>();
     private int idHotel;
     private int idCiudad;
     private String nombre;
     private String descripcion;
     private String tarjeta;
     private String ubicacion;
     private String direccion;
     private String web;
     private int calificacion;
     private String fotoHotel;
     private String latitud;
     private String longitud;

    public Hotel(int idHotel, int idCiudad, String nombre, String descripcion, String tarjeta, String ubicacion, String direccion, String web, int calificacion, String fotoHotel, String latitud, String longitud) {
        this.idHotel = idHotel;
        this.idCiudad = idCiudad;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.tarjeta = tarjeta;
        this.ubicacion = ubicacion;
        this.direccion = direccion;
        this.web = web;
        this.calificacion = calificacion;
        this.fotoHotel = fotoHotel;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public int getIdHotel() {
        return idHotel;
    }

    public int getIdCiudad() {
        return idCiudad;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getWeb() {
        return web;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public String getFotoHotel() {
        return fotoHotel;
    }

    public String getLatitud() {
        return latitud;
    }

    public String getLongitud() {
        return longitud;
    }
     
    /*Este metodo llena la lista de hoteles desde el archivo*/
    public static void llenarListaHoteles() throws IOException{
        try(BufferedReader r = new BufferedReader(new FileReader("src/datos/hoteles1.csv"));){
            String Linea;
            r.readLine();
            while((Linea = r.readLine()) != null ){
                String[] s = Linea.split("\\|");
                int idHotel = Integer.parseInt(s[0]);
                int idCiudad = Integer.parseInt(s[1]);
                int califica = Integer.parseInt(s[8]);
                hoteles.add(new Hotel(idHotel,idCiudad,s[2],s[3],s[4],s[5],s[6],s[7],califica,s[9],s[10],s[11]));
            } 
        }catch(FileNotFoundException ex){
            System.out.println("No se encontro el archivo hoteles1.csv "+ex);            
        }
    }
    /*este metodo da una lista de hoteles que tiene esa ciudad
    utilizado en la clase busquedaProvincia*/
    public static List<Hotel> filtroporCiudad(int idciu){
        List<Hotel> listahoteles = new ArrayList<>();
        for(Hotel ho: hoteles){
            if(ho.idCiudad == idciu){
                listahoteles.add(ho);
            }
        }
        return listahoteles;
    }
    
    /*este metodo entra una lista de nombre de hotel*/
    public static List<Hotel> filtroporNombre(String nombre){
        List<Hotel> listahotelesnombre = new ArrayList<>();
        for(Hotel ho: hoteles){
            if(ho.nombre.equals(nombre)){
                listahotelesnombre.add(ho);
            }
        }
        return listahotelesnombre;
    }
    
    //metodo devuelve una lista de hoteles por la id
    public static List<Hotel> filtroporid(int id){
        List<Hotel> listahotelesnombre = new ArrayList<>();
        for(Hotel ho: hoteles){
            if(ho.idHotel == id){
                listahotelesnombre.add(ho);
            }
        }
        return listahotelesnombre;
    }
    
    public static List<Hotel> filtroporservicio(String nombre){
        List<Hotel> listahoteles = new ArrayList<>();
        for (Integer in : Servicio.filtrohotelservicio(nombre)){
            for(Hotel ho: hoteles){
            if(ho.idHotel == in){
                listahoteles.add(ho);
            }
           }
        }
        return listahoteles;
    }
   
    /*este metodo da un array de string con los nombres de los hoteles existentes¨*/
    public static String[] arrayhotel(){
        String hotelesnombres[] = new String[hoteles.size()];
        int i = 0;
        for(Hotel hot: hoteles){
            if (i <= hoteles.size()){
                hotelesnombres[i] = hot.nombre;
                i += 1;
            }  
        }
        return hotelesnombres;
    }

    
    @Override
    public String toString() {
        return nombre;
    }
    
    public String getNombreCuidad(){
        for (Ciudad ciudad : Ciudad.ciudades){
            if(this.idCiudad == ciudad.getIdCiudad()){
                return ciudad.toString();
            }
        }
        return "";
    }
}
 
    