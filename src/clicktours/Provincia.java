/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clicktours;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Esta es la clase provincia
 * 
 * @author Gilson Ponce Briones
 */
public class Provincia {
    public static List<Provincia> provincias = new ArrayList<>();
    private int idProvincia;
    private String provincia;
    private String descripcion;
    private String region;
    private String web;

    public Provincia(int idProvincia, String provincia, String descripcion, String region, String web) {
        this.idProvincia = idProvincia;
        this.provincia = provincia;
        this.descripcion = descripcion;
        this.region = region;
        this.web = web;
    }

    public String getProvincia() {
        return provincia;
    }

    public String getDescripcion() {
        return descripcion;
    }

   /*Este metodo llena la lista de provincia desde el archivo*/
   public static void llenarListaProvincia() throws IOException{
       try(BufferedReader r = new BufferedReader(new FileReader("src/datos/provincias.csv"));){
            String linea;
            r.readLine();
            while((linea = r.readLine())!= null){
                String[] s = linea.split("\\|");
                int id = Integer.parseInt(s[0]);
                provincias.add(new Provincia(id,s[1],s[2],s[3],s[4]));
            }
        } catch (FileNotFoundException ex) {
            System.out.println("No se encontro el archivo provincias.csv"+ex);
        }
   }
   /**
    * Este metodo retorna la id de la provincia
    * escogida en la ventana busqueda por provincia
    */
   public static int idProvincia(String p){
     int i = 0;
     for(Provincia pro: provincias){
         if((pro.provincia).equals(p)){
             return pro.idProvincia;
         }
     }
     return i;  
   }

    @Override
    public String toString() {
        return provincia;
    }
   
   
    
}
