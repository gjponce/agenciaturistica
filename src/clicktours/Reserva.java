/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clicktours;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Alfredo Valenzuela
 */
public class Reserva implements Serializable{
//    private Date fechaEntrada;
    private int numeroPersonas;
    private String tipoHabitacion;

    private Date fechaReserva;
    private Cliente cliente;

    private String hotel;
    private double valorPagar;
    private double comision;
    public  static ArrayList<Reserva> reservas= new ArrayList<>();
    

    public Reserva(int numeroPersonas, String  tipoHabitacion, Date fechaReserva,Cliente cliente,String hotel, double  valorPagar, double comision){

 
        this.numeroPersonas = numeroPersonas;
        
        this.tipoHabitacion=tipoHabitacion;
        this.cliente=cliente;
        this.hotel= hotel;
        this.valorPagar=valorPagar;
        this.comision=comision;  
        this.fechaReserva=fechaReserva;
    }

    @Override
    public String toString() {

        return "numeroPersonas=" + numeroPersonas + ", tipoHabitacion=" + tipoHabitacion + ", fechaReserva=" + fechaReserva + ", cliente=" + cliente + ", Nombre Hotel :"+hotel+", valorPagar=" + valorPagar + ", comision=" + comision ;

    }

    public int getNumeroPersonas() {
        return numeroPersonas;
    }

    public String getTipoHabitacion() {
        return tipoHabitacion;
    }

    public Date getFechaReserva() {
        return fechaReserva;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public double getValorPagar() {
        return valorPagar;
    }

    public double getComision() {
        return comision;
    }

    public static ArrayList<Reserva> getReservas() {
        return reservas;
    }

    public String getHotel() {
        return hotel;
    }



  

}
