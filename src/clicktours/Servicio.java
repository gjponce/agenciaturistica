/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clicktours;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Esta calse llena la lista servicios desde el archivo
 * @author Gilson Ponce Briones
 */
public class Servicio {
     public static List<Servicio> servicios = new ArrayList<>();
     private int secuencia;
     private int idHotel;
     private int idServicio;
     private String estado;

    public Servicio(int secuencia, int idHotel, int idServicio, String estado) {
        this.secuencia = secuencia;
        this.idHotel = idHotel;
        this.idServicio = idServicio;
        this.estado = estado;
    }

    public int getSecuencia() {
        return secuencia;
    }

    public int getIdHotel() {
        return idHotel;
    }

    public int getIdServicio() {
        return idServicio;
    }

    public String getEstado() {
        return estado;
    }
     
     /*Este metodo llena la lista de servicios desde el archivo*/
    public static void llenarListaServicios() throws IOException{
        try(BufferedReader r = new BufferedReader(new FileReader("src/datos/servicios.csv"));){
            String Linea;
            r.readLine();
            while((Linea = r.readLine()) != null ){
                String[] s = Linea.split("\\|");
                int secu = Integer.parseInt(s[0]);
                int idHotel = Integer.parseInt(s[1]);
                int idServicio = Integer.parseInt(s[2]);
                servicios.add(new Servicio(secu,idHotel,idServicio,s[3]));
            } 
        }catch(FileNotFoundException ex){
            System.out.println("No se encontro el archivo servcios.csv "+ex);            
        }
    }
    
    //devuelve una lista con la id de hoteles
    public static List<Servicio> servicioHotel(int idhotel){
        List<Servicio> serviciohotel = new ArrayList<>();
        for(Servicio ser: servicios){
            if(ser.idHotel == idhotel){
                serviciohotel.add(ser);
            }
        }
        return serviciohotel;
    }
    
    public static List<Integer> filtrohotelservicio(String nombre){
        List<Integer> idshoteles = new ArrayList<>();
        for(Integer ij: Catalogo.listaservicioidnombre(nombre)){
            for(Servicio ser: servicios){
                if(ij == ser.idServicio){
                    idshoteles.add(ser.idHotel);
                }
            }
        }
        return idshoteles;
    }
   
    @Override
    public String toString() {
        return estado;
    }
    
    
    
}
