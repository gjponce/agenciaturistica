/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clicktours;

import static clicktours.Provincia.provincias;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Esta clase llena la lista de ciudades desde el archivo
 * @author Gilson Ponce Briones 
 */
public class Ciudad {
     public static List<Ciudad> ciudades = new ArrayList<>();
     private int idCiudad;
     private int idProvincia;
     private String ciudad;

    public Ciudad(int idCiudad, int idProvincia, String ciudad) {
        this.idCiudad = idCiudad;
        this.idProvincia = idProvincia;
        this.ciudad = ciudad;
    }

     /*Este metodo llena la lista de ciudades desde el archivo*/
    public static void llenarListaCiudades() throws IOException{
        try(BufferedReader r = new BufferedReader(new FileReader("src/datos/ciudades.csv"));){
            String Linea;
            r.readLine();
            while((Linea = r.readLine()) != null ){
                String[] s = Linea.split("\\|");
                int idCiu = Integer.parseInt(s[0]);
                int idPro = Integer.parseInt(s[1]);
                ciudades.add(new Ciudad(idCiu,idPro,s[2]));
            } 
        }catch(FileNotFoundException ex){
            System.out.println("No se encontro el archivo catalogo.csv "+ex);            
        }
    }
    /**
     * este metodo entrga una lista de las ciudades que tiene
     * la provincia, escogida en la ventana busqueda provincia
     * ciudad
    */
    public static List<Ciudad> listaCiudadProvincia(int id){
        List<Ciudad> ciudadProvincia = new ArrayList<>();
        for(Ciudad ciu: ciudades){
            if(ciu.idProvincia == id){
                ciudadProvincia.add(ciu);
            }
        }
        return ciudadProvincia;
    }
    
   public static int idCiudad(String c){
     int i = 0;
     for(Ciudad ciu: ciudades){
         if((ciu.ciudad).equals(c)){
             return ciu.idCiudad;
         }
     }
     return i;  
   }

    @Override
    public String toString() {
        return ciudad;
    }

    public int getIdCiudad() {
        return idCiudad;
    }

    
    
}
