/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clicktours;


import GUI.menuPrincipal;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;


import java.io.IOException;



/**
 *
 * @author Gilson Ponce Briones
 */
public class Clicktours extends Application {
    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException{
        // TODO code application logic here
        llenarListas();
        launch();  
    }
    
    /*Llena las listas de los archivos csv*/
    public static void llenarListas() throws IOException{
        Catalogo.llenarListaCatalogos();
        Ciudad.llenarListaCiudades();
        Habitacion.llenarListaHabitaciones();
        Hotel.llenarListaHoteles();
        Provincia.llenarListaProvincia();
        Servicio.llenarListaServicios();
    }

 @Override
 public void start(Stage s){
     Scene sc = new Scene(new menuPrincipal().getPrincipal(),400,300);
     s.setScene(sc);
     s.setTitle("Menu Principal");  
     s.show();
 }
}
