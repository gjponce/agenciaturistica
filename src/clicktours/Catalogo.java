/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clicktours;
import static clicktours.Hotel.hoteles;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * Esta clase lee el archivo de catalogos y agrega los objetos a la lista
 * @author Gilson Ponce Briones
 */
public class Catalogo {
    public static List<Catalogo> catalogos = new ArrayList<>();
    private int idServicio;
    private String servicio;

    public Catalogo(int idServicio, String servicio) {
        this.idServicio = idServicio;
        this.servicio = servicio;
    }
    
    /*Este metodo llena la lista de catalogo ´7desde el archivo*/
    public static void llenarListaCatalogos() throws IOException{
        try(BufferedReader r = new BufferedReader(new FileReader("src/datos/catalogo.csv"));){
            String Linea;
            r.readLine();
            while((Linea = r.readLine()) != null ){
                String[] s = Linea.split("\\|");
                int id = Integer.parseInt(s[0]);
                catalogos.add(new Catalogo(id,s[1]));
            } 
        }catch(FileNotFoundException ex){
            System.out.println("No se encontro el archivo catalogo.csv "+ex);            
        }
    }
    
    /* este metodo entrega una lista de servicio que coincida con la id*/
    public static List<Catalogo> listaServicioId(int idservicio){
        List<Catalogo> serviciosid = new ArrayList<>();
        for (Catalogo cat: catalogos){
            if(cat.idServicio == idservicio){
                serviciosid.add(cat);
            }
        }
        return serviciosid;
    }

    /* este metodo entrega un String de los nombre de servicios*/
    public static String[] arrayservicios(){
        String serviciosnombres[] = new String[catalogos.size()];
        int i = 0;
        for(Catalogo ca: catalogos){
            serviciosnombres[i] = ca.servicio;
            i += 1;
        }
        return serviciosnombres;
    }
    //retorna una lista con las id que coincida con el nombre del servicio
    public static List<Integer> listaservicioidnombre(String nombre){
        List<Integer> idservicios = new ArrayList<>();
        for (Catalogo cat: catalogos){
            if(cat.servicio.equals(nombre)){
                idservicios.add(cat.idServicio);
            }
        }
        return idservicios;
    }
    
    public int getIdServicio() {
        return idServicio;
    }

    public String getServicio() {
        return servicio;
    }
    @Override
    public String toString() {
        return servicio;
    }
    
    
}
