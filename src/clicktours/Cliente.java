/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clicktours;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * esta calse tiene la lista de clientes y el metodo para serializar dicha lista
 * creando un archivo en recursos cliente0.dat
 * @author Gilson
 */
public class Cliente implements Serializable{
    public static List<Cliente> clientes = new ArrayList<>();
    private int cedula;
    private String nombre;
    private String direccion;
    private String correo;

    public Cliente(int cedula, String nombre, String direccion, String correo) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.direccion = direccion;
        this.correo = correo;
    }
    
    public static void serializar(){
        try(ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream("src/datos/cliente0.dat"))){
            o.writeObject(clientes);
        }catch(IOException e){
            System.out.println("Error en serializacion "+e);
        }
    }

    /*public static void deserializar() throws ClassNotFoundException{
        try(ObjectInputStream i = new ObjectInputStream(new FileInputStream("src/datos/cliente0.dat"))){
            ArrayList<Cliente> recuperado =(ArrayList<Cliente>)i.readObject(); 
        }catch(IOException e){
            System.out.println("No se encontro el archivo"+e);
        }
    }*/
    
    @Override
    public String toString() {
        return nombre ;
    }

    public static List<Cliente> getClientes() {
        return clientes;
    }

    public int getCedula() {
        return cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getCorreo() {
        return correo;
    }
    
}
