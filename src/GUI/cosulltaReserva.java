/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import clicktours.Reserva;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author Alfredo Valenzuela
 */
public class cosulltaReserva {
     private static VBox root = new VBox();
     private Button bt1= new Button("Reservar por Fecha");
       private Button bt2= new Button("Reservar por Hotel");
        private Button bt3= new Button("Reservar por cliente");
        private TextField tf1= new TextField();
        private TextField tf2= new TextField();
        private TextField tf3= new TextField();
       private  HBox hb1= new HBox(10);
        private HBox hb2= new HBox(10);
        private HBox hb3= new HBox(10);
        private VBox vb= new VBox(20);
        public static ArrayList<Reserva> porFecha= new ArrayList<>();
         public static ArrayList<Reserva> porCliente= new ArrayList<>();
          public static ArrayList<Reserva> porHotel= new ArrayList<>();
        public static ArrayList<Reserva> reservas1= new ArrayList<>();
        
        
        public  cosulltaReserva(){
            crearVentana();
        }
         public void crearVentana(){
        root.setPadding(new Insets(25));
        root.setAlignment(Pos.CENTER);
         root.getChildren().add(vb);
         vb.setAlignment(Pos.CENTER);
         vb.getChildren().addAll(hb1,hb2,hb3);
         hb1.getChildren().addAll(tf1,bt1);
           hb2.getChildren().addAll(tf2,bt2);
             hb3.getChildren().addAll(tf3,bt3);
             bt1.setOnAction(e-> busquedaPorFecha());
              bt3.setOnAction(e-> busquedaPorCliente());
              bt2.setOnAction(e-> busquedaPorHotel());
        }


         public void busquedaPorFecha(){
                 try( ObjectInputStream oi= new ObjectInputStream(new FileInputStream("src/datos/reservas.dat"))){
                     reservas1=(ArrayList<Reserva>)oi.readObject();
                     Date fecha= busquedaHotelServicio.ParseFecha(tf1.getText());
                     for(Reserva r: reservas1){
                         if(r.getFechaReserva().equals(fecha)){
                             porFecha.add(r);
                         }
                     }
                 } catch (FileNotFoundException ex) {
             Logger.getLogger(cosulltaReserva.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IOException ex) {
             
             Logger.getLogger(cosulltaReserva.class.getName()).log(Level.SEVERE, null, ex);
         } catch (ClassNotFoundException ex) {
             Logger.getLogger(cosulltaReserva.class.getName()).log(Level.SEVERE, null, ex);
         }
                 System.out.println( porFecha);
                 tf1.clear();
                 }
         
         //SE BUSCAN LAS RESERVAS POR NOMBRE DEL HOTEL 
           public void busquedaPorHotel(){
                 try( ObjectInputStream oi= new ObjectInputStream(new FileInputStream("src/datos/reservas.dat"))){
                     reservas1=(ArrayList<Reserva>)oi.readObject();
                     for(Reserva r: reservas1){
                         if(r.getHotel().equals(tf2.getText())){
                             porHotel.add(r);
                         }
                     }
                 } catch (FileNotFoundException ex) {
             Logger.getLogger(cosulltaReserva.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IOException ex) {
             
             Logger.getLogger(cosulltaReserva.class.getName()).log(Level.SEVERE, null, ex);
         } catch (ClassNotFoundException ex) {
             Logger.getLogger(cosulltaReserva.class.getName()).log(Level.SEVERE, null, ex);
         }
                 System.out.println( porHotel);
                 tf2.clear();
                 }
           //SE BUSCAN LAS RESERVAS POR NOMBRE DEL CLIENTE 
         public void busquedaPorCliente(){
                 try( ObjectInputStream oi= new ObjectInputStream(new FileInputStream("src/datos/reservas.dat"))){
                     reservas1=(ArrayList<Reserva>)oi.readObject();
                     String s= tf3.getText();
                     for(Reserva r: reservas1){
                         if(r.getCliente().getNombre().equals(s)){
                             porCliente.add(r);
                         }
                     }
                 } catch (FileNotFoundException ex) {
             Logger.getLogger(cosulltaReserva.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IOException ex) {
             
             Logger.getLogger(cosulltaReserva.class.getName()).log(Level.SEVERE, null, ex);
         } catch (ClassNotFoundException ex) {
             Logger.getLogger(cosulltaReserva.class.getName()).log(Level.SEVERE, null, ex);
         }
                 System.out.println( porCliente);
                 tf3.clear();
                 }

    public static VBox getRoot() {
        return root;
    }

    public Button getBt1() {
        return bt1;
    }

    public Button getBt2() {
        return bt2;
    }

    public Button getBt3() {
        return bt3;
    }





         
}
