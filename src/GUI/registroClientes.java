/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;


import clicktours.Cliente;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javax.swing.JOptionPane;

/**
 * Esta clase da la opcion de registrar clientes con sus datos
 * y una vez que se registra la lista se serializa llamando 
 * al metodo en clientes
 * @author Gilson Ponce Briones
 */
public class registroClientes {
//inserta un espacion entre los nodos
private VBox  root =new VBox(20);
private HBox hb1= new HBox();
private HBox hb2= new HBox();
private HBox hb3= new HBox();
private HBox hb4= new HBox();
private HBox hb5 = new HBox(20);
private Button bt= new Button("Registrar");
private TextField tf1= new TextField();
private TextField tf2= new TextField();
private TextField tf3= new TextField();
private TextField tf4= new TextField();
private Label lb1= new Label("Cedula    : ");
private Label lb2= new Label("Nombre    : ");
private Label lb3= new Label("Direccion : ");
private Label lb4= new Label("Correo    : ");

public registroClientes(){
    organizacionVentana();
}

/*loyaut de la ventana*/
public void organizacionVentana(){
    //inserta un espacio entra la venta y el control
     root.setPadding(new Insets(25));
     root.setAlignment(Pos.CENTER);
     //minimo de ancho de cada textfiel
     tf1.setMinWidth(200);
     tf2.setMinWidth(200);
     tf3.setMinWidth(400);
     tf4.setMinWidth(400);
     hb5.setAlignment(Pos.CENTER);
     root.getChildren().addAll(hb1,hb2,hb3,hb4,hb5);
     hb1.getChildren().addAll(lb1,tf1);
     hb2.getChildren().addAll(lb2,tf2);
     hb3.getChildren().addAll(lb3,tf3);
     hb4.getChildren().addAll(lb4,tf4);
     hb5.getChildren().addAll(bt);
     bt.setOnAction(e -> registrarCliente());
     
}

/*registra a los clientes en la lista de clientes que se encuentra en el objeto Cliente*/
 public void registrarCliente(){

    try{  
      
       int cedula = Integer.parseInt(tf1.getText());
       String nombre = tf2.getText();
       String direccion = tf3.getText();
       String correo = tf4.getText();
      Cliente c= new Cliente(cedula,nombre,direccion,correo);
       if(!tf1.getText().equals(" ") & !tf2.getText().equals(" ") & !tf3.getText().equals(" ")& !tf4.getText().equals(" ")){
        System.out.println("entre al if");
        Cliente.clientes.add(c);
        System.out.println(c);
       JOptionPane.showMessageDialog(null, "Registro exitoso", "Registro de cliente", JOptionPane.DEFAULT_OPTION); 
       Cliente.serializar();
        }
        else{
          JOptionPane.showMessageDialog(null, "Ingrese todos los valores, por favor", "Registro de cliente", JOptionPane.WARNING_MESSAGE);  
     }
    }
     catch(NumberFormatException e){
           tf1.setText("");
           JOptionPane.showMessageDialog(null, "Ingrese valores numericos", "Registro de cliente", JOptionPane.WARNING_MESSAGE);
      }    
      System.out.println(Cliente.clientes);
     
       tf1.clear();
       tf2.clear();
       tf3.clear();
        tf4.clear();
  }
 
 public VBox getRoot() {
        return root;
 }
}
