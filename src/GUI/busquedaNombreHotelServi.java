/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import clicktours.Catalogo;
import clicktours.Habitacion;

import clicktours.Hotel;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javax.swing.JProgressBar;
import org.controlsfx.control.textfield.TextFields;

/**
 *
 * @author Usuario
 */
public class busquedaNombreHotelServi{
    private VBox principal = new VBox(20);
    private VBox principal1 = new VBox(20);
    private VBox hotel = new VBox(10);
    private VBox servicio = new VBox(10);
    private Label lbltitulo1 = new Label("Escriba el nombre de Hotel");
    private TextField txthotel = new TextField();
    private TextField txtservicio = new TextField();
    private Label lbltitulo2 = new Label("Escriba el servicio");
    private Button btnbuscarhotel = new Button("Buscar");
    private Button btnbuscarservicioho = new Button("Buscar");
    

    public busquedaNombreHotelServi() {
        formatear();    
    }
    
    public VBox getPrincipal() {
        return principal;
    }
    
    public void formatear(){
        principal.setPadding(new Insets(10));
        principal.setAlignment(Pos.CENTER);
        hotel.setAlignment(Pos.CENTER);
        servicio.setAlignment(Pos.CENTER);
        String[] h = Hotel.arrayhotel();
        String[] s = Catalogo.arrayservicios();
        TextFields.bindAutoCompletion(txthotel,h);
        TextFields.bindAutoCompletion(txtservicio,s);
        hotel.getChildren().addAll(lbltitulo1,txthotel,btnbuscarhotel);
        servicio.getChildren().addAll(lbltitulo2,txtservicio,btnbuscarservicioho);
        principal.getChildren().addAll(hotel,servicio);
        btnbuscarhotel.setOnAction(e -> {
            presentarHotel(txthotel.getText().trim());
            txtservicio.setText("");
                });
        btnbuscarservicioho.setOnAction(e -> {
            presentarHotelServicio(txtservicio.getText().trim());
            txtservicio.setText("");
                });
        //btnbuscarservicioho.setOnAction(e -> )
    }
    
    public void presentarHotel(String nombre){
        ScrollPane panel = new ScrollPane();
        panel.getChildrenUnmodifiable().clear();
        Stage ventana = new Stage();
        busquedaHotelServicio busqueda =  new busquedaHotelServicio(nombre,Hotel.filtroporNombre(nombre),panel);
        Scene sc2 = new Scene(panel,700,200);
        ventana.close();
        ventana.setScene(sc2);
        ventana.setTitle("Busqueda por Nombre de Hotel");
        ventana.show();
    } 
    
    public void presentarHotelServicio(String nombre){
        ScrollPane panel = new ScrollPane();
        panel.getChildrenUnmodifiable().clear();
         Stage ventana = new Stage();
         busquedaHotelServicio busqueda = new busquedaHotelServicio(nombre,Hotel.filtroporservicio(nombre),panel);
         Scene sc2 = new Scene(panel,700,400);
        ventana.close();
        ventana.setScene(sc2);
        ventana.setTitle("Busqueda por Nombre Servicio");
        ventana.show();
    } 
}
