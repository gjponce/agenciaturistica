/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import static GUI.busquedaHotelServicio.ParseFecha;
import static GUI.busquedaHotelServicio.calcularPrecio;
import static GUI.busquedaHotelServicio.crearReserva;
import static GUI.busquedaHotelServicio.hb4;
import static GUI.busquedaHotelServicio.tf;
import clicktours.Catalogo;
import clicktours.Cliente;
import clicktours.Habitacion;
import clicktours.Hotel;
import clicktours.Reserva;
import clicktours.Servicio;
import java.awt.Desktop;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Pair;
import javax.swing.JOptionPane;

/**
 *
 * @author Lucio Arias
 */
public class busquedaMasCercanos {
    private VBox principal = new VBox(20);
    private ScrollPane panel = new ScrollPane();
    List<Pair<Hotel,Double>> hoteles_cercanos;
    static  TextField  tf= new TextField();
    static Label valorHabitacion= new Label("Valor Habitacion");
   static  HBox hb4= new HBox();
     private static  TextField fReserva= new TextField();
       static  ComboBox habitacion= new ComboBox();
       static ComboBox cliente= new ComboBox();
       private static Label lb1;
   
   
    public busquedaMasCercanos(List<Pair<Hotel,Double>> cercanos) {
        this.hoteles_cercanos=cercanos;
        presentarListaHoteles(this.hoteles_cercanos);
    }

    public ScrollPane getPanel() {
        return panel;
    }
    
    //metodo encargado de mostrar el listado de hoteles de la ciudad
    public void presentarListaHoteles(List<Pair<Hotel,Double>> hoteles){
        principal.getChildren().clear();
        for(Pair<Hotel,Double> hotel_distancia : hoteles){
            Image image = new Image(hotel_distancia.getKey().getFotoHotel());
            ImageView foto = new ImageView(image);
            foto.setFitWidth(170);
            foto.setFitHeight(120);
            Label lblnombrehotel = new Label(hotel_distancia.getKey().getNombre());
            Label lbldireccion = new Label(hotel_distancia.getKey().getDireccion());
            //dependiendo de la calificacion del hotel son las estrellas
            String estrellas = "";
            int estre = hotel_distancia.getKey().getCalificacion();
            if(estre == 5){
                estrellas = "★★★★★";
            } else if(estre == 4){
                estrellas = "★★★★☆";
            }else if(estre == 3){
                estrellas = "★★★☆☆";
            }else if(estre == 2){
                estrellas = "★★☆☆☆";
            }else if(estre == 1){
                estrellas = "★☆☆☆☆";
            }else{
                estrellas = "☆☆☆☆☆";
            }
            Label lblstrellas = new Label(estrellas);
            lblstrellas.setTextFill(Color.ROYALBLUE);
            lblstrellas.setMaxSize(180,180);
            Button btninfo = new  Button("+ Información");
            btninfo.setOnAction(e -> btninfo(hotel_distancia.getKey()));
            Button btnmapa = new Button("Ver Mapa");
            btnmapa.setOnAction(e -> btnmapa(hotel_distancia.getKey(), hotel_distancia.getKey().getNombreCuidad()));
            Button btnreservar = new Button("Reservar");
            HBox Botones = new HBox(10);
            VBox Secunda = new VBox(10);
            HBox Princi = new HBox(15);
            Botones.getChildren().addAll(btninfo,btnmapa,btnreservar);
            btnreservar.setOnAction(e->ventanaReserva(hotel_distancia.getKey()));
            Secunda.getChildren().addAll(lblnombrehotel,lbldireccion,lblstrellas,Botones);
            Princi.getChildren().addAll(foto,Secunda);
            principal.getChildren().add(Princi);
            
        }
        principal.setAlignment(Pos.CENTER);
        principal.setPadding(new Insets(20));
        panel.setContent(principal);
        panel.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        panel.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        //panel.setPannable(true); permite dezaplazarnos dentro del panel
    }
        
    

    /*muestra la ventana de informacion del Hotel seleccionado*/
    public static void btninfo(Hotel h){
        Stage ven = new Stage();
        VBox habitacion = new VBox(10);
        VBox servicio = new VBox(10);
        VBox padre = new VBox(20);
        //se hace uso del ScrollPane para poder ver toda la informacion
        ScrollPane scroll = new ScrollPane();
        padre.setPadding(new Insets(25));
        int idhotel = h.getIdHotel();
        //llena un VBox con el tipo de habitacion y sus precios
        for(Habitacion ha: Habitacion.listaHabiHotel(idhotel)){
            
            VBox habitacion2 = new VBox();
            HBox detalle = new HBox(10);
            HBox detalle2 = new HBox(10);
            HBox detalle3 = new HBox(10);
            Label lbltipohabitacion = new Label(ha.getTipoHabitacion());
            Label lblpreciossimple = new Label(String.valueOf(ha.getTarifaSim()));
            Label lblpreciosdoble = new Label(String.valueOf(ha.getTarifaDou()));
            Label lblpreciostriple = new Label(String.valueOf(ha.getTarifaTri()));
            detalle.getChildren().addAll(new Label("Tarifa Simple: "),lblpreciossimple);
            detalle2.getChildren().addAll(new Label("Tarifa Dobles: "),lblpreciosdoble);
            detalle3.getChildren().addAll(new Label("Tarifa Triple: "),lblpreciostriple);
            habitacion2.getChildren().addAll(lbltipohabitacion,detalle,detalle2,detalle3);
            habitacion.getChildren().add(habitacion2);
            
        }
        //llena un VBox con los servcios del hotel
        for(Servicio serv: Servicio.servicioHotel(idhotel)){
            for(Catalogo cata: Catalogo.listaServicioId(serv.getIdServicio())){
                Label lblservicio = new Label(cata.getServicio());
                VBox vser = new VBox(10);
                vser.getChildren().add(lblservicio);
                servicio.getChildren().add(vser);
            }
        }
        Label lbldescripcionhotel = new Label(h.getDescripcion());
        lbldescripcionhotel.setWrapText(true);
         lbldescripcionhotel.setMaxWidth(450);
        Label lblwebhotel = new Label(h.getWeb());
        padre.getChildren().addAll(new Label("Habitaciones: "),habitacion,new Label("Servicios: "),servicio,new Label("Descripcion: "),lbldescripcionhotel,new Label("Web: "),lblwebhotel);
        padre.setAlignment(Pos.CENTER);
        scroll.setContent(padre);
        Scene s = new Scene(scroll,500,500);
        ven.setTitle("Información Del Hotel "+h.getNombre());
        ven.setScene(s);
        ven.show();
    }
    
    //metodo encargado de abrir el navegador en google maps con la ubicacion del hottel
    public static void btnmapa(Hotel ho, String Ciudad){
         URI myURI;
         try {
             String nombre = ho.getNombre();
             //los espacios del nombre del hotel son reemplazados por %20
             String palabras = nombre.replace(" ", "%20");
             String query= palabras+"+"+Ciudad+"%20Ecuador";
             myURI = new URI("https://www.google.com/maps/search/?api=1&query="+query);
             Desktop.getDesktop().browse(myURI);
         } catch (URISyntaxException ex) {
             JOptionPane.showMessageDialog(null, "Error al escribir la URL", "Google Maps", JOptionPane.WARNING_MESSAGE);
         } catch (IOException ex) {
             JOptionPane.showMessageDialog(null, "Error el llamar al navegador", "Google Maps", JOptionPane.WARNING_MESSAGE);
         }
    }
        public   void ventanaReserva(Hotel h){
        if(Cliente.clientes.size()!=0){
        Stage ventana= new Stage();
     VBox reservas= new VBox(20);
     HBox hb= new HBox(10);
    HBox hb4= new HBox(10);
    HBox hb1= new HBox(10);
    HBox hb2= new HBox(10);
    HBox hb3= new HBox(10);
    
    HBox hb5= new HBox(10);
    reservas.setPadding(new Insets(20));
    Button bt= new Button("Reservar");

    Label fecha= new Label("Fecha de Reserva");
   
    Label nHotel = new Label("Hotel:");
    Label tipoHabitacion= new Label("Tipo Habitacion");
    Label npersonas= new Label("Numero Personas:");
    
    
    



    reservas.getChildren().addAll(hb,hb1,hb2,hb3,hb4,hb5);
    hb5.setAlignment(Pos.CENTER);

    lb1= new Label(h.getNombre());
    String nombreH = lb1.getText();// obtengo el nombre del hotel 
        hb.getChildren().addAll(new Label(" Cliente: " ),cliente);
        
        cliente.getItems().addAll(Cliente.clientes);// para poder hacer la reserva con el cliente registrado necesario
         
        hb1.getChildren().addAll(nHotel,lb1);
        hb2.getChildren().addAll(tipoHabitacion,habitacion);
        habitacion.getItems().addAll(Habitacion.listaHabiHotel(h.getIdHotel()));
        
        System.out.println(Habitacion.listaHabiHotel(h.getIdHotel()));
        
        hb3.getChildren().addAll(npersonas,tf);

        hb4.getChildren().addAll(fecha,fReserva);

       // obtengo el numero de personas  por habitacion y el precio 
     
    //________________________________

        hb5.getChildren().add(bt);
        bt.setAlignment(Pos.CENTER);
       
        
        Scene sc= new Scene(reservas,400,300);
        ventana.setTitle("Reserva "+ h.getNombre());
        ventana.setScene(sc);
        ventana.show();
        

    
               
  
       bt.setOnAction(e->{
           
     crearReserva(h);
     serializar();
        });
               }
        else{
            JOptionPane.showMessageDialog(null, "No existe cliente para realizar reserva", "Error", JOptionPane.WARNING_MESSAGE);  
        }
       
        
  
    }
    
    public static int obtenerPersonas(){
        int nPersonas =Integer.parseInt(tf.getText());
        return nPersonas;
    }
    public static void serializar(){
        try (ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream("src/datos/reservas.dat"))) {
            o.writeObject(Reserva.reservas);
        } catch (IOException e) {
            System.out.println("Error en serializar()" + e);
        }
    }
    public static double calcularComision(double precio){
        double comision= (precio*10.0)/100.0;
        return comision;
    }
      public static void crearReserva(Hotel h){
        Cliente c = (Cliente) cliente.getValue();
        String tipoH= ((Habitacion) habitacion.getValue()).getTipoHabitacion();
        double precio=calcularPrecio(h);
        int  nPersonas=obtenerPersonas();
        double comision= calcularComision(precio);
        Date fechaReserva= ParseFecha(fReserva.getText());
        String nombreH = lb1.getText();
        Reserva r = new Reserva(nPersonas,tipoH,fechaReserva,c,nombreH,precio,comision);
        Reserva.reservas.add(r);
        System.out.println(r);
 JOptionPane.showMessageDialog(null, "Reserva Exitosa: :)", "Google", JOptionPane.WARNING_MESSAGE);
       
    }
    
    public VBox getPrincipal() {
        return principal;
    }

    public void setPrincipal(VBox principal) {
        this.principal = principal;
    }

    public void setPanel(ScrollPane panel) {
        this.panel = panel;
    }   
}