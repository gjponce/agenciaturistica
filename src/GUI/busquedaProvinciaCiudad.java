/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;
import clicktours.Ciudad;
import clicktours.Hotel;
import clicktours.Provincia;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * Esta clase dara a escojer al usuario la provincia y la ciudad de esa provincia, en la cual le mostrar la descripcion
 * y dara la opcion en un boton de buscar un hotel en la ciudad
 * @author Gilson Ponce Briones
 */
public class busquedaProvinciaCiudad {
    private static Stage windows = new Stage();
    private VBox principal1 = new VBox(20);
    private HBox secundario1 = new HBox(15);
    private HBox secundario2 = new HBox(15);
    private ComboBox cboprovincia = new ComboBox();
    private ComboBox cbociudad = new ComboBox();
    private Button btnbuscarHotel = new Button("Buscar Hotel");
    private Label lbldescripcion = new Label();
    private Label lblprovincia = new Label("Elija la provincia: ");
    private Label lblciudad = new Label("Elija la ciudad: ");
    private Label lblsms = new Label("en esta ciudad de esta provincia con sus servicios");

    public busquedaProvinciaCiudad() {
        formatear();
        llenarProvincia();
    }

    public VBox getPrincipal1() {
        return principal1;
    }
    /*metodo que da el loyaut a la ventana con acciones*/
    public void formatear(){
        principal1.setPadding(new Insets(25));
        principal1.setAlignment(Pos.CENTER);
        lbldescripcion.setWrapText(true);
        lbldescripcion.setPadding(new Insets(10));
        cboprovincia.setCursor(Cursor.HAND);
        cbociudad.setCursor(Cursor.HAND);
        cbociudad.setDisable(true);
        btnbuscarHotel.setDisable(true);
        secundario1.getChildren().addAll(lblprovincia,cboprovincia);
        secundario2.getChildren().addAll(lblciudad,cbociudad);
      
        principal1.getChildren().addAll(secundario1,lbldescripcion,secundario2,btnbuscarHotel,lblsms);
        
        //llama dos metodos
        cboprovincia.setOnAction(e -> {
        llenarCiudad();
        LlenarDescripcion();
        });
        cbociudad.setOnAction(e -> btnbuscarHotel.setDisable(false));
        btnbuscarHotel.setOnAction(e -> {
            llamarVentanaHoteles();
        });
    }
   
   /*llena el combo de la provincias*/
   public void llenarProvincia(){
       cboprovincia.getItems().clear();
       //extrae las provincias de la lista del Objeto Provincia
       cboprovincia.getItems().addAll(Provincia.provincias);
   }
   
   /*llena el combo de la ciudad, dependiendo de la provincia
   hace el juego de desactivar los controles*/
   public void llenarCiudad(){
      cbociudad.getItems().clear();
      btnbuscarHotel.setDisable(true);
      String seleccion = cboprovincia.getSelectionModel().getSelectedItem().toString();
      int idprovincia = Provincia.idProvincia(seleccion);
      cbociudad.getItems().addAll(Ciudad.listaCiudadProvincia(idprovincia));
      cbociudad.setDisable(false);
   }
   /*metodo que llena la descripcion de la provincia*/ 
   public void LlenarDescripcion(){
       lbldescripcion.setText("");
       String selec = cboprovincia.getSelectionModel().getSelectedItem().toString();
       for(Provincia prov: Provincia.provincias){
           if(prov.getProvincia().equals(selec)){
              lbldescripcion.setText(prov.getDescripcion());
           }
       }
   }
   
   /*llama la ventana de mostrar las lista de hoteles de la ciudad*/
   public void llamarVentanaHoteles(){
      ScrollPane panel = new ScrollPane();
      panel.getChildrenUnmodifiable().clear();
      windows.show();
      String seleccion = cbociudad.getSelectionModel().getSelectedItem().toString();
      int idciudadd = Ciudad.idCiudad(seleccion);
      busquedaHotelServicio busqueda = new busquedaHotelServicio(seleccion,Hotel.filtroporCiudad(idciudadd),panel);
      Scene sc2 = new Scene(panel,600,500);
      windows.setScene(sc2);
      windows.setTitle("Hoteles");
     
   }
}
