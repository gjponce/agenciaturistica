 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;


import clicktours.Reserva;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import clicktours.Clicktours;
import javafx.application.Application;
import javafx.application.Platform;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.Window;



/**
 * Esta clase sera el menu principal del proyecto
 * donde se poner a dispoción al usuario la diferentes opciones 
 * del programa
 * @author Gilson
 */
public class menuPrincipal extends Clicktours{
    private VBox principal = new VBox(25);
    private static Button btnRegistrarCliente = new Button("Registrar Cliente");
    private static Button btnBusqueda1 = new Button("Busqueda por provincia y ciudad");
    private static Button btnBusqueda2 = new Button("Busqueda por Hotel y Servicios");
    private static Button consultar= new Button("Consultar Reservas");
    private Button btn1= new Button("Busqueda por lugar turistico");
    private Label lbltitulo = new Label("Menú Principal");
    private static Stage ventana = new Stage();

   
    private ArrayList <Reserva>reservas1= new ArrayList<>();

    private static Stage ventana1 = new Stage();
    private Clicktours menu = new Clicktours();

    
    public menuPrincipal(){
        organizar();
    }
    
    public void mostrarmenu(){
     Scene sc = new Scene(principal,500,500);
     ventana.setScene(sc);
     ventana.setTitle("Menu Principal");  
     ventana.show();
    }
    public static Button getBtnRegistrarCliente() {
        return btnRegistrarCliente;
    }

    public static Button getBtnBusqueda1() {
        return btnBusqueda1;
    }

    
    
    public void organizar(){
        principal.setPadding(new Insets(25));
        principal.setAlignment(Pos.CENTER);

        principal.getChildren().addAll(lbltitulo,btnRegistrarCliente,btnBusqueda1,btnBusqueda2,btn1,consultar);


      
        btnRegistrarCliente.setOnAction(e -> mostrarRegistro());
        btnBusqueda1.setOnAction(e -> mostrarBusquedaProvincia());
        btn1.setOnAction(e -> mostrarPorTuris());
        btnBusqueda2.setOnAction(e-> mostrarBusquedaHotel());
   

    
        btnRegistrarCliente.setOnAction(e -> {
           Window win = btnRegistrarCliente.getScene().getWindow();
           win.hide();
           mostrarRegistro();
                });
        
        btnBusqueda1.setOnAction(e -> {
            Window win = btnRegistrarCliente.getScene().getWindow();
           win.hide();
            mostrarBusquedaProvincia();
                });
        btn1.setOnAction(e -> {
            Window win = btnRegistrarCliente.getScene().getWindow();
           win.hide();
            mostrarPorTuris();
                });
        btnBusqueda2.setOnAction(e-> {
            Window win = btnRegistrarCliente.getScene().getWindow();
           win.hide();
            mostrarBusquedaHotel();
                });
        consultar.setOnAction(e->consultarReservas());
        ventana.setOnCloseRequest(eh -> start(ventana1));

    }
    
    public void mostrarRegistro(){
        Scene sc1 = new Scene(new registroClientes().getRoot());
        ventana.close();
        ventana.setScene(sc1);
        ventana.setTitle("Resgistro de clientes");
        ventana.show();
        
    }
    
    public void mostrarBusquedaProvincia(){
        Scene sc2 = new Scene(new busquedaProvinciaCiudad().getPrincipal1(),800,400);
        ventana.close();
        ventana.setScene(sc2);
        ventana.setTitle("Busqueda por Provincia y Ciudad");
        ventana.show();
    }
    
    public void mostrarBusquedaHotel(){
        Scene sc4= new Scene(new busquedaNombreHotelServi().getPrincipal(),500,300);
        ventana.close();
        ventana.setScene(sc4);
        ventana.setTitle("Busqueda por Nombre de Hotel y Servicios");
        ventana.show();
    }
    public void mostrarPorTuris(){
        Scene sc3= new Scene(new busquedaTuristico().getTuristico(),400,200);
        ventana.close();
        ventana.setScene(sc3);
        ventana.setTitle("Busqueda por Lugar Turistico");
        ventana.show();    
    }
    public void consultarReservas(){
        Scene sc5= new Scene(new cosulltaReserva().getRoot(),500,300);
        ventana.close();
        ventana.setScene(sc5);
        ventana.setTitle("Busqueda de Reservas");
        ventana.show();    
    }
    public static void salirregistro(){
       ventana.close();
    }

    public VBox getPrincipal() {
        return principal;
    }
    public void obtenerReservas(){
        try(ObjectInputStream oi= new ObjectInputStream(new FileInputStream("src/datos/reservas.dat"))){
           reservas1=(ArrayList<Reserva>) oi.readObject();
            System.out.println(reservas1);
        }catch(IOException ex){
            System.out.println("error al deseruializar");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(menuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
    
