package GUI;


import clicktours.Hotel;
import generadorhtmlmapbox.GeneradorHTMLMapBox;
import generadorhtmlmapbox.Ubicacion;
import geocoding.GeoCoding;
import java.awt.Desktop;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Pair;

/**
 *
 * @author Alfredo Valenzuela
 * ESTA CLASE PERMITE EL INGRESO DE UN LUGAR TURISTICO Y DEVUELVO LOS 5 HOTLES MAS CERCANOS A EL 
 * ADEMAS DE UN MAPA CON LOS LUGARES EXACTOS DE CADA UNO
 *
 */
public class busquedaTuristico {
    private static Stage windows = new Stage();
    private VBox turistico = new VBox(15);
    private Label lb= new Label("Ingrese un Lugar Turistico");
    private TextField tf= new TextField();
    private Button bt= new Button("Buscar");
    private Button bt2= new Button("Salir");
    private String TOKEN = "pk.eyJ1IjoiYWxmcmVkb2VjdSIsImEiOiJjazVyNHRpZzAwOWpsM2ttanEza3hyZHUxIn0.bK0fI8cA9oNFUbbBuVzC6A";
   
    public busquedaTuristico(){
        mostrarVentana();
         
    }


    public void mostrarVentana(){
        turistico.getChildren().addAll(lb,tf,bt);
        turistico.setAlignment(Pos.CENTER);
        turistico.setPadding(new Insets(20));
        bt.setOnAction(e->buscarLugar());

    }    

        public VBox getTuristico() {
            return turistico;
        }
      
    public void buscarLugar(){
        try{
            GeoCoding aw = new GeoCoding(TOKEN);
            Ubicacion u = aw.consultar(tf.getText());
            System.out.println("Latitud: "+u.getLatitud());
            System.out.println("Longitud: "+u.getLongitud());
            List<Pair<Hotel,Double>> cercanos = mas_cercanos(u);
            GeneradorHTMLMapBox b = new GeneradorHTMLMapBox(TOKEN,u.getLongitud(),u.getLatitud());
            for (Pair<Hotel, Double> cercano : cercanos) {
                System.out.println(cercano);
                b.anadirMarcador(Double.parseDouble(cercano.getKey().getLongitud()), Double.parseDouble(cercano.getKey().getLongitud()),cercano.getKey().getNombre());
            }
            b.grabarHTML("src/HTML/mapa.html");
            //Abriendo html generado con MAPBOX
            Desktop.getDesktop().browse(Paths.get("src/HTML/mapa.html").toAbsolutePath().toUri());
            filtro_cercanos(cercanos);
        }
        catch(IOException ex){
            Logger.getLogger(busquedaTuristico.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public List<Pair<Hotel,Double>> mas_cercanos(Ubicacion busqueda){
        List<Pair<Hotel,Double>> resultado = new ArrayList<>();
        int i=0;
        for(Hotel h : Hotel.hoteles){
            if (!h.getLongitud().trim().equals("-")){ 
                if(!h.getLatitud().trim().equals("-")){
                    resultado.add(new Pair<Hotel,Double>(h,calculateDistanceByHaversineFormula(busqueda,h.getLongitud().trim(),h.getLatitud().trim())));
                }
            }
            i++;
        }
            resultado.sort(new Comparator<Pair<Hotel, Double>>() {
            @Override
            public int compare(Pair<Hotel, Double> o1, Pair<Hotel, Double> o2) {
                if (o1.getValue() < o2.getValue()) {
                    return -1;
                } else if (o1.getValue().equals(o2.getValue())) {
                    return 0;
                } else {
                    return 1;
                }
            }
        });
        
        return resultado.subList(0, 5);
    }
//CONVIERTE LA LATITUD Y LA LONGITUD EN  KM PARA ENCONTRAR LOS HOTELES MAS CERCANSO
    
    private double calculateDistanceByHaversineFormula(Ubicacion busqueda,
        String objetivo_long, String objetivo_lat) {

        double earthRadius = 6378.137; // km

        double lat1 = busqueda.getLatitud();
        double lon1 = busqueda.getLongitud();
        double lat2 = Double.parseDouble(objetivo_lat);
        double lon2 = Double.parseDouble(objetivo_long);

        double dlon = (lon2 - lon1)*Math.PI/180;
        double dlat = (lat2 - lat1)*Math.PI/180;

        double sinlat = Math.sin(dlat / 2);
        double sinlon = Math.sin(dlon / 2);

        double a = (sinlat * sinlat) + Math.cos(lat1*Math.PI/180)*Math.cos(lat2*Math.PI/180)*(sinlon*sinlon);
        double c = 2 * Math.asin (Math.min(1.0, Math.sqrt(a)));

        double distanceInMeters = earthRadius * c ;

        return distanceInMeters;

    }
    
    public void filtro_cercanos(List<Pair<Hotel,Double>> cercanos){
        Scene sc2 = new Scene(new busquedaMasCercanos(cercanos).getPanel(),600,500);
        windows.setScene(sc2);
        windows.setTitle("Mas cercanos");
        windows.show();
    }

}


